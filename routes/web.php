<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

Route::get('/',[StudentController::class,'index']);
Route::post('/formsubmit',[StudentController::class,'add_data']);
Route::get('/table',[StudentController::class,'show']);
Route::get('/delete/{id}',[StudentController::class,'delete']);
Route::get('view/edit/{id}',[StudentController::class,'edit']);
Route::post('/update/{id}',[StudentController::class,'update']);
