<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\student1;
use PHPUnit\Framework\MockObject\Builder\Stub;

class StudentController extends Controller
{
    public function index()
    {
        return view('form');
    }

    // public function add_data(Request $request){
    //     // dd($request->all());
    //     $request->validate([
    //         'name' => 'required',
    //         'lname' => 'required',
    //         'email' => 'required|email',
    //         'phone' => 'required',
    //         'address' => 'required',
    //         'city' => 'required',
    //         'state' => 'required',
    //         'country' => 'required',
    //         'dob' => 'required',
    //         'gender' => 'required',
    //         'des' => 'required',
    //     ], ['required' => 'This field is required']);
    //     $data = new Student;
    //     $data->name = $request->input('name');
    //     $data->lname = $request->input('lname');
    //     $data->email = $request->input('email');
    //     $data->phone = $request->input('phone');
    //     $data->address = $request->input('address');
    //     $data->city = $request->input('city');
    //     $data->state = $request->input('state');
    //     $data->country = $request->input('country');
    //     $data->dob = $request->input('dob');
    //     $data->gender = $request->input('gender');
    //     $data->des = $request->input('des');
    //     $data->save();

    //     $urduData = [
    //         'name' => 'نام',
    //         'lname' => 'آخری نام',
    //         'email' => 'ایمیل',
    //         'phone' => 'فون',
    //         'address' => 'پتہ',
    //         'city' => 'شہر',
    //         'state' => 'ریاست',
    //         'country' => 'ملک',
    //         'dob' => 'تاریخ پیدائش',
    //         'gender' => 'جنس',
    //         'des' => 'شرح',
    //     ];
    //     $data = new Student1;
    //     $data->name = $request->input('name');
    //     $data->lname = $request->input('lname');
    //     $data->email = $request->input('email');
    //     $data->phone = $request->input('phone');
    //     $data->address = $request->input('address');
    //     $data->city = $request->input('city');
    //     $data->state = $request->input('state');
    //     $data->country = $request->input('country');
    //     $data->dob = $request->input('dob');
    //     $data->gender = $request->input('gender');
    //     $data->des = $request->input('des');
    //     foreach ($urduData as $key => $value) {
    //         $data->$key = $request->input($key);
    //     }
    //     $data->save();
    //     return response()->json([
    //         'status' => true,
    //         'done' => "Student Has Been Inserted Successfully"
    //     ]);
    // }

    public function add_data(Request $request) {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'des' => 'required',
        ],['required'=>'This field is required']);
        $data = new Student;
        $data->name = $request->input('name');
        $data->lname = $request->input('lname');
        $data->email = $request->input('email');
        $data->phone = $request->input('phone');
        $data->address = $request->input('address');
        $data->city = $request->input('city');
        $data->state = $request->input('state');
        $data->country = $request->input('country');
        $data->dob = $request->input('dob');
        $data->gender = $request->input('gender');
        $data->des = $request->input('des');
        $data->save();

        $urduData = [
            'name' => 'نام',
            'lname' => 'آخری نام',
            'email' => 'ایمیل',
            'phone' => 'فون',
            'address' => 'پتہ',
            'city' => 'شہر',
            'state' => 'ریاست',
            'country' => 'ملک',
            'dob' => 'تاریخ پیدائش',
            'gender' => 'جنس',
            'des' => 'شرح',
        ];
        $dataUrdu = new Student1;
        foreach ($urduData as $key => $label) {
            $dataUrdu->$key = $request->$key; // Assigning English data to corresponding Urdu field
        }
        $dataUrdu->save();
        return response()->json([
            'status'=>true,
            'done'=>"Student Has Been Inserted Successfully"
        ]);
    }

    public function show()
    {
        $data = Student::all();
        return view('table')->with(compact('data'));
    }

    public function delete($id)
    {
        $data = Student::find($id);
        $data->delete();
        return redirect()->back()->with('danger', "Student Table has been Deleted");
    }

    public function edit($id)
    {
        $data = Student::find($id);
        return view('update')->with(compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Student::find($id);
        $data->name = $request['name'];
        $data->lname = $request['lname'];
        $data->email = $request['email'];
        $data->phone = $request['phone'];
        $data->address = $request['address'];
        $data->city = $request['city'];
        $data->state = $request['state'];
        $data->country = $request['country'];
        $data->dob = $request['dob'];
        $data->gender = $request['gender'];
        $data->des = $request['des'];
        $data->save();
        return redirect('/table')->with('success', 'Student Has been updated');
    }
}
