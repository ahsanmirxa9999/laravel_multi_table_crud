<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <!-- Title Page-->
    <title>Laravel Multi Language</title>
    <!-- Icons font CSS-->
    <link href="./assets/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="./assets/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
        rel="stylesheet">
    <!-- Vendor CSS-->
    <link href="./assets/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="./assets/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    <!-- Main CSS-->
    <link href="./assets/css/main.css" rel="stylesheet" media="all">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" type="text/css"href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

</head>

<body>
    <div class="page-wrapper p-t-180 p-b-100 font-robo">
        <div class="wrapper wrapper--w960">
            <div class="card card-2">
                <div class="card-heading"></div>
                <div class="card-body">
                    <div class="row row-space">
                        <h2 class="title">Student Form</h2>
                        <div class="col-2">
                            <a class="btn btn--green" href="{{ url('/table') }}" role="button"
                                style="margin-left:300px;">View Form</a>
                        </div>
                    </div>
                    <form method="POST" action="" id="myform" enctype="multipart/form-data">
                        @csrf
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="text" placeholder="First Name"
                                        name="name">
                                    <span style="color: red" id="nameerror"></span>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="text" placeholder="Last Name" name="lname">
                                    <span style="color: red" id="lnameerror"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="email" placeholder="Email" name="email">
                                    <span style="color: red" id="emailerror"></span>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="number" placeholder="Phone Number"
                                        name="phone">
                                    <span style="color: red" id="phoneerror"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="text" placeholder="Address" name="address">
                                    <span style="color: red" id="addresserror"></span>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="text" placeholder="Country" name="country">
                                    <span style="color: red" id="countryerror"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="text" placeholder="State" name="state">
                                    <span style="color: red" id="stateerror"></span>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2" type="text" placeholder="City" name="city">
                                    <span style="color: red" id="cityerror"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-2 js-datepicker" type="text"
                                        placeholder="Birthdate" name="dob">
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    <span style="color: red" id="doberror"></span>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="gender">
                                            <option disabled="disabled" selected="selected">Gender</option>
                                            <option>Male</option>
                                            <option>Female</option>
                                            <option>Other</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-group">
                            <input type="text" name="des" class="input--style-2"
                                placeholder="Type Description">
                            <span style="color: red" id="deserror"></span>
                        </div>
                        <div class="p-t-30">
                            <button class="btn btn--radius btn--green" id="btnssss" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="./assets/vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="./assets/vendor/select2/select2.min.js"></script>
    <script src="./assets/vendor/datepicker/moment.min.js"></script>
    <script src="./assets/vendor/datepicker/daterangepicker.js"></script>
    <!-- Main JS-->
    <script src="./assets/js/global.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

</body>
</html>

<script>
    $('#btnssss').on('click', function(e) {
        // console.log(e);
        e.preventDefault();
        let formData = new FormData(myform)
        // console.log(mydata)
        $.ajax({
            url: "{{ URL('/formsubmit') }}",
            method: "POST",
            contentType: false,
            processData: false,
            data: formData,
            success: function(res) {
                if (res.done) {
                    toastr.success(res.done, 'success!', {
                        timeOut: 2000
                    })
                }
                $('#myform').trigger('reset');
                // alert('Student is inserted Successfully');
            },
            error: function(error) {
                console.log(error.responseJSON.errors.name);
                // Clear previous error messages
                document.getElementById('nameerror').innerText = '';
                document.getElementById('lnameerror').innerText = '';
                document.getElementById('emailerror').innerText = '';
                document.getElementById('phoneerror').innerText = '';
                document.getElementById('addresserror').innerText = '';
                document.getElementById('cityerror').innerText = '';
                document.getElementById('stateerror').innerText = '';
                document.getElementById('countryerror').innerText = '';
                document.getElementById('doberror').innerText = '';
                document.getElementById('deserror').innerText = '';
                if (error.responseJSON.errors.name) {
                    document.getElementById('nameerror').innerText = error.responseJSON.errors.name[
                        0];
                }
                if (error.responseJSON.errors.lname) {
                    document.getElementById('lnameerror').innerText = error.responseJSON.errors
                        .lname[0];
                }
                if (error.responseJSON.errors.email) {
                    document.getElementById('emailerror').innerText = error.responseJSON.errors
                        .email[0];
                }
                if (error.responseJSON.errors.phone) {
                    document.getElementById('phoneerror').innerText = error.responseJSON.errors
                        .phone[0];
                }
                if (error.responseJSON.errors.address) {
                    document.getElementById('addresserror').innerText = error.responseJSON.errors
                        .address[0];
                }
                if (error.responseJSON.errors.city) {
                    document.getElementById('cityerror').innerText = error.responseJSON.errors
                        .city[0];
                }
                if (error.responseJSON.errors.state) {
                    document.getElementById('stateerror').innerText = error.responseJSON.errors
                        .state[0];
                }
                if (error.responseJSON.errors.country) {
                    document.getElementById('countryerror').innerText = error.responseJSON.errors
                        .country[0];
                }
                if (error.responseJSON.errors.dob) {
                    document.getElementById('doberror').innerText = error.responseJSON.errors.dob[
                        0];
                }
                if (error.responseJSON.errors.des) {
                    document.getElementById('deserror').innerText = error.responseJSON.errors.des[
                        0];
                }
            }
        })
    });
</script>
{{--
<script>
    @if (Session::has('success'))
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        }
        toastr.success("{{ session('success') }}", 'success!', {
            timeOut: 2000
        })
    @endif
</script> --}}

</html>
